<?php

require_once 'config.php';
require_once 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$pdo = new PDO(DB_CONNECTION_STRING, DB_USER, DB_PASS);

$config = array(
    'environments' => array(
        'default_migration_table' => 'dg_phinxlog',
        'default_database' => 'development',
        'development' => array(
            'name' => 'devdb',
            'connection' => $pdo
        )
    ),
    'paths' => array(
        'migrations' => "%%PHINX_CONFIG_DIR%%/src/Migration",
        'seeds' => "%%PHINX_CONFIG_DIR%%/src/Migration/seed"
    )
);

return $config;