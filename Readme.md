```postgresplsql
DROP TABLE IF EXISTS docs;
CREATE TABLE docs (
  id serial NOT NULL,
  name text NOT NULL,
  year integer NOT NULL
) PARTITION BY RANGE (year);

INSERT INTO docs (name, year) VALUES
  ('a', 2016),
  ('aa', 2016),
  ('aaa', 2016),
  ('b', 2017),
  ('bb', 2017),
  ('bbb', 2017);

CREATE TABLE docs_y2016
PARTITION OF docs
FOR VALUES FROM (2016) TO (2016);

CREATE TABLE docs_y2017
PARTITION OF docs
FOR VALUES FROM (2017) TO (2017);

CREATE TABLE docs_y2018
PARTITION OF docs
FOR VALUES FROM (2018) TO (2018);

INSERT INTO docs (name, year) VALUES
  ('c', 2016),
  ('d', 2017),
  ('e', 2018);

EXPLAIN ANALYSE SELECT * FROM docs WHERE year = 2016;
EXPLAIN ANALYSE SELECT * FROM docs WHERE year = 2017;
EXPLAIN ANALYSE SELECT * FROM docs WHERE year = 2018;
```