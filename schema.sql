-- era przed partycjonowaniem
CREATE TABLE docs (
  id serial NOT NULL,
  name text NOT NULL,
  year integer NOT NULL
) PARTITION BY RANGE (year);

CREATE TABLE files (
  id SERIAL NOT NULL ,
  docs_id INT NOT NULL REFERENCES docs(id),
  content text NULL
);

INSERT INTO docs (id, name, year) VALUES
  (1, 'a', 2016),
  (2, 'aa', 2016),
  (3, 'aaa', 2016),
  (4, 'b', 2017),
  (5, 'bb', 2017),
  (6, 'bbb', 2017);

INSERT INTO files (docs_id, content) VALUES
  (1, 'x'),  (1, 'x'),  (1, 'x'),
  (2, 'y'),  (2, 'y'),  (2, 'y'),
  (3, 'z'),  (3, 'z'),  (3, 'z'),
  (4, 'q'),  (4, 'q'),  (4, 'q'),
  (5, 'w'),  (5, 'w'),  (5, 'w'),
  (6, 'e'),  (6, 'e'),  (6, 'e');

-- wprowadzamy partycjonowanie

-- DOCS
CREATE TABLE docs_y2016
PARTITION OF docs
FOR VALUES FROM (2016) TO (2016);

CREATE TABLE docs_y2017
PARTITION OF docs
FOR VALUES FROM (2017) TO (2017);

CREATE TABLE docs_y2018
PARTITION OF docs
FOR VALUES FROM (2018) TO (2018);

INSERT INTO docs (name, year) VALUES
  ('c', 2016),
  ('d', 2017),
  ('e', 2018);


-- FILES


EXPLAIN ANALYSE SELECT * FROM docs WHERE year = 2016;
EXPLAIN ANALYSE SELECT * FROM docs WHERE year = 2017;
EXPLAIN ANALYSE SELECT * FROM docs WHERE year = 2018;